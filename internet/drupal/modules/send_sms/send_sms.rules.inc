<?php
/**
 * Implement hook_rules_action_info().
 */
 
function send_sms_rules_action_info() {
    global $base_url; 
	
	$actions = array(
		'send_sms_rules_action_send_sms' => array(
		  'label'=> t('Send SMS'),
		  'group' => t('Send SMS'),
		  'parameter' => array(
			'thesender' => array(
			  'type' => 'text',
			  'label' => t('The SMS Sender'),
			  'optional' => TRUE,
			  'description' => t('The sender name. Leave empty to use the site-wide sender name as set in the settings page <a href="@home/@s">here</a> .', array('@s' => 'admin/config/user-interface/send-sms','@home' => $base_url))
			),
			'thebody' => array(
			  'type' => 'text',
		      'label' => t('The SMS message')
			),
			'thenumber' => array(
			  'type' => 'text',
			  'label' => t('The SMS Recipient'),
			)
		  ),
		)
	);
  
  
	return $actions; 
  
}

// The callback to send the SMS
function send_sms_rules_action_send_sms($thesender, $thebody, $thenumber) {
	
	//get the variables stored in the settings form 
	$theusername = variable_get('send_sms_username_value', '');
	$sms_username_label = variable_get('send_sms_username_label', '');
	$thepassword = variable_get('send_sms_password_value', '');
	$sms_password_label = variable_get('send_sms_password_label', '');
	$sms_message_label = variable_get('send_sms_message_label', '');
	$sms_sender_label = variable_get('send_sms_sender_label', '');
	$sms_recipient_label = variable_get('send_sms_recipient_label', '');
	$show_info = variable_get('send_sms_show_info', '');
	$sms_base_url = variable_get('send_sms_base_url', '');
	
	//encode the text field for the message 
	//$themessage = rawurlencode($thebody);
	$themessage = $thebody;
	
	if(empty($thesender)){
		$thesender_value = variable_get('send_sms_sender_value', '');
	}else{
		$thesender_value = $thesender;
	}
	
	//use http_build_query
	$parameters = array(
      $sms_username_label => $theusername,
      $sms_password_label => $thepassword,
      $sms_message_label => $themessage,
      $sms_sender_label => $thesender_value,
      $sms_recipient_label => $thenumber
    );
	
	$url_params = http_build_query($parameters);
	$url = $sms_base_url. "?".$url_params;
	
	// use drupal drupal_http_request to send a post request to the API.
	
	$create_message = drupal_http_request(
	  $url, array(
	    'headers' => array('Content-Type' => 'application/json'),
	    'method' => 'POST'
	  ));
	  
	if($show_info == 1){
		drupal_set_message($create_message->data);
	}

//dpm($list_item);
}