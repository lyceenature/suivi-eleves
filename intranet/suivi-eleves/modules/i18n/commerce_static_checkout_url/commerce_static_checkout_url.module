<?php

/**
 * @file
 * Provides helper functions for redirection of several banking institues
 */

define('CSCU_GB_REVIEW_AND_PROCEED', 'checkout/static/payment');
define('CSCU_ORDER_SESSION_TAG', 'remember_order_id');

/**
 * Implements hook_menu().
 */
function commerce_static_checkout_url_menu() {
  $items = array();

  $items[CSCU_GB_REVIEW_AND_PROCEED] = array(
    'title' => 'Redirect to Payment Bank',
    'page callback' => 'commerce_static_checkout_url_pay_to_be_altered',
    'page arguments' => array(),
    'access callback' => 'commerce_static_checkout_url_completion_access',
    'type' => MENU_CALLBACK,
  );

  $items['admin/commerce/config/static_checkout_url'] = array(
    'title' => 'Static Checkout URL settings',
    'description' => 'Configuration for static url on checkout',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_static_checkout_url_settings_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Make sure anyone can complete their orders through our rediction modules.
 */
function commerce_static_checkout_url_completion_access() {
  return TRUE;
}

/**
 * Hook settings form.
 */
function commerce_static_checkout_url_settings_form($form, &$form_state) {
  global $base_url;
  $form['bank_links'] = array(
    '#type' => 'fieldset',
    '#description' => t('Use the following links when you contact your bank.'),
    '#title' => t('Bank links'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['bank_links']['from'] = array(
    '#type' => 'item',
    '#title' => t('Proceed to bank'),
    '#markup' => url($base_url . '/' . CSCU_GB_REVIEW_AND_PROCEED, array('absolute' => TRUE)),
  );

  $at_least_one = FALSE;
  foreach (module_invoke_all('static_checkout_url_links') as $title => $link) {
    $at_least_one = TRUE;
    $form['bank_links'][$title] = array(
      '#type' => 'item',
      '#title' => check_plain($title),
      '#markup' => url($base_url . '/' . $link, array('absolute' => TRUE)),
    );
  }

  if ($at_least_one == FALSE) {
    $form['bank_links']['No other links found'] = array(
      '#type' => 'item',
      '#title' => t('NOTICE: No active bank modules were found!'),
      '#markup' => t('You should enable at least one bank module to communicate with a bank. Please go to the modules page and enable the one corresponding to the bank you want.'),
    );
  }

  $form['static_checkout_url_offsite_auto_redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto redirect to external gateway'),
    '#default_value' => variable_get('static_checkout_url_offsite_auto_redirect', TRUE),
    '#description' => t('Check this if you want your clients automatically redirected to the external bank gateway.'),
  );

  $default_text_redirect = t('Please wait while you are redirected to the secure payment server. If nothing happens within 10 seconds, please click on the button below.');
  $form['static_checkout_url_proceed_text'] = array(
    '#type' => 'textarea',
    '#maxlength' => 512,
    '#title' => t('Help text'),
    '#default_value' => variable_get('static_banks_proceed_text', $default_text_redirect),
    '#description' => t('This text will be shown with the Proceed button'),
    '#required' => TRUE,
  );

  $text = 'Redirect here after succesful payment. Type &ltfront&gt for the frontpage. Default value is !url.';
  $form['static_checkout_url_redirect_after_success'] = array(
    '#type' => 'textfield',
    '#maxlength' => 256,
    '#title' => t('Auto redirect user after succesfull payment'),
    '#field_prefix' => $base_url . '/',
    '#default_value' => variable_get('static_checkout_url_redirect_after_success', CSCU_GB_REVIEW_AND_PROCEED),
    '#description' => t($text, array('!url' => CSCU_GB_REVIEW_AND_PROCEED)),
    '#required' => TRUE,
  );

  $default_text_success = t('Order was succesfully paid');
  $form['static_checkout_url_message_after_success'] = array(
    '#type' => 'textarea',
    '#maxlength' => 256,
    '#title' => t('Message to show after succesfull payment'),
    '#default_value' => variable_get('static_checkout_url_message_after_success', $default_text_success),
    '#description' => t('Show message after succesfull payment.'),
    '#required' => TRUE,
  );

  $default_text_cancel = t('Your payment was cancelled. Please feel free to contact us for assistance or continue shopping.');
  $form['static_checkout_url_message_after_cancel'] = array(
    '#type' => 'textarea',
    '#maxlength' => 256,
    '#title' => t('Message to show after cancelation'),
    '#default_value' => variable_get('static_checkout_url_message_after_cancel', $default_text_cancel),
    '#description' => t('Show message, after user cancels payment'),
  );

  $form['static_checkout_url_redirect_after_error'] = array(
    '#type' => 'textfield',
    '#maxlength' => 256,
    '#title' => t('Auto redirect user after error'),
    '#field_prefix' => $base_url . '/',
    '#default_value' => variable_get('static_checkout_url_redirect_after_error', '<front>'),
    '#description' => t('Redirect here if an error occurs. Type &ltfront&gt for the frontpage or [checkout-order-review-page] for the order review page.'),
    '#required' => TRUE,
  );

  $default_text_error = t('Payment has failed with the following error message: @Error');
  $form['static_checkout_url_message_after_error'] = array(
    '#type' => 'textarea',
    '#maxlength' => 256,
    '#title' => t('Message to show after error'),
    '#default_value' => variable_get('static_checkout_url_message_after_error', $default_text_error),
    '#description' => t('Show message after error. <strong>@Error</strong> will be replaced with the error returned from the bank.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_hook_info().
 */
function commerce_static_checkout_url_hook_info() {
  $hooks = array(
    'static_checkout_url_links' => array(
      'group' => 'commerce',
    ),
  );
  return $hooks;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function commerce_static_checkout_url_form_commerce_checkout_form_payment_alter(&$form, &$form_state, $form_id) {
  if (!isset($form['bank'])) {
    return;
  }

  $default_text_proceed = t('Use the button below to proceed to the payment server.');
  $text = variable_get('static_checkout_url_proceed_text', $default_text_proceed);
  $form['help']['#markup'] = '<div class="checkout-help">' . $text . '</div>';
}


/**
 * Implements hook_url_outbound_alter().
 */
function commerce_static_checkout_url_url_outbound_alter(&$path, &$options, $original_path) {
  global $base_url, $language;

  $parts = explode('/', $original_path);
  if (count($parts) == 3) {
    if (drupal_strtolower($parts[0]) == 'checkout' && drupal_strtolower($parts[2]) == 'payment') {
      $_SESSION[CSCU_ORDER_SESSION_TAG] = $parts[1];
      $_SESSION[_commerce_static_checkout_url_get_language_session_param()] = $language->prefix;
      
      $path = $base_url . '/' . CSCU_GB_REVIEW_AND_PROCEED;
      $options['external'] = TRUE;
    }
  }
}

/**
 * Implements hook_url_inbound_alter().
 */
function commerce_static_checkout_url_url_inbound_alter(&$path, $original_path, $path_language) {
  if (CSCU_GB_REVIEW_AND_PROCEED == $original_path) {
    $path = 'checkout/' . $_SESSION[CSCU_ORDER_SESSION_TAG] . '/payment';
  }
}

/**
 * Implements hook_help().
 */
function commerce_static_checkout_url_help($path, $arg) {
  switch ($path) {
    case "admin/help#commerce_static_checkout_url":
      $output = '<p>';
      $output .= t("Core module for Commerce Static URLs on checkout, it is required by other modules.");
      $output .= '</p>';
      $output .= t("Creates a static url on the final checkout step in order to communicate with several banks. ");
      $output .= t("It also displays the URLs needed from the bank to create an e-commerce account and provides some
      options like auto-redirect and success/fail/cancel messages.");
      $output .= '</p>';
      return $output;
  }
}

/**
 * Helper load order or die function for external modules.
 */
function commerce_static_checkout_url_load_order_or_die($order_id) {
  $order = commerce_order_load($order_id);

  if ($order === FALSE) {
    commerce_static_checkout_url_redirect_with_error('bankname', NULL, 'Error with order number', TRUE);
  }
  return $order;
}

/**
 * Helper error and redirect function for external modules.
 */
function commerce_static_checkout_url_redirect_with_error($module, $order, $error_message, $force_no_message = FALSE) {
  $default_text_error = t('Payment has failed with the following error message: @Error');
  watchdog('commerce_static_checkout_url',
    $default_text_error . ' [IP: @ip][Bank: @bank]',
    array(
      '@order_id' => $order->order_id,
      '@Error' => $error_message,
      '@ip' => ip_address(),
      '@bank' => $module,
    ),
    WATCHDOG_WARNING);

  $text = variable_get('static_checkout_url_message_after_error', $default_text_error);
  if ($text != '' && $force_no_message == FALSE) {
    drupal_set_message(t($text, array('@Error' => $error_message)));
  }

  global $base_url, $language;
  $pr = '';
  if (isset($_SESSION[_commerce_static_checkout_url_get_language_session_param()])) {
    $pr = '/' . $_SESSION[_commerce_static_checkout_url_get_language_session_param()];
  }

  $link = variable_get('static_checkout_url_redirect_after_error', '<front>');
  if ($link == '[checkout-order-review-page]' && $order != NULL) {
    drupal_goto($base_url . $pr . '/checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key']);
  }
  else if ($link == '<front>') {
    drupal_goto($base_url . $pr);
  }
  else {
    drupal_goto($base_url . $pr . '/' . $link);
  }
}

/**
 * Helper cancel and redirect function for external modules.
 */
function commerce_static_checkout_url_redirect_with_cancel($module, $order, $cancel_message = '', $force_no_message = FALSE) {
  $default_textcancel = t('Your payment was cancelled. Please feel free to continue shopping or contact us for assistance.');

  $text = variable_get('static_checkout_url_message_after_cancel', $default_text_cancel);
  if ($text != '' && $force_no_message == FALSE) {
    drupal_set_message(t($text, array('@Cancel' => $cancel_message)));
  }

  global $base_url, $language;
  $pr = '';
  if (isset($_SESSION[_commerce_static_checkout_url_get_language_session_param()])) {
    $pr = $_SESSION[_commerce_static_checkout_url_get_language_session_param()] . '/';
  }

  drupal_goto($base_url . '/' . $pr . 'checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key']);
}

/**
 * Helper success and redirect function for external modules.
 */
function commerce_static_checkout_url_redirect_with_success($module, $success_message = '', $force_no_message = FALSE) {
  $default_text_success = t('Order was succesfully paid');

  $text = variable_get('static_checkout_url_message_after_success', $default_text_success);
  if ($text != '' && $force_no_message == FALSE) {
    drupal_set_message(t($text, array('@Success' => $success_message)));
  }

  global $base_url, $language;
  $pr = '';
  if (isset($_SESSION[_commerce_static_checkout_url_get_language_session_param()])) {
    $pr = '/' . $_SESSION[_commerce_static_checkout_url_get_language_session_param()];
  }
  $link = variable_get('static_checkout_url_redirect_after_success', CSCU_GB_REVIEW_AND_PROCEED);
  if ($link == '<front>') {
    drupal_goto($base_url . $pr);
  }
  else {
    drupal_goto($base_url . $pr . '/' . $link);
  }
}

/**
 * Function for external modules.
 */
function commerce_static_checkout_url_get_auto_redirect() {
  return variable_get('static_checkout_url_offsite_auto_redirect', TRUE);
}

function _commerce_static_checkout_url_get_language_session_param() {
  // Get the language parameter as defined here admin/config/regional/language/configure/session
  return variable_get('locale_language_negotiation_session_param', 'language');
}
