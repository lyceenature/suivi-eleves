DESCRIPTION
-----------

SEND SMS MODULE provides a rules action for you to send SMS for any rules event. the action exposes the parameters you would want to set. These are often the sms text, recipient phone number, and the sender name. This module uses the drupal_http_request to send post request to the sms url.

INSTALLATION
------------

Install as normal Drupal module and enable


CONFIGURATION
-------------

- The configuration page of this module is located at Administration > Config > User Interface > Send SMS (admin/user-interface/send-sms).

The configuration page allows you to set the following fields.

 * SMS website base url: This is the base url of the SMS api. (everything before the '?'). e.g 'http://smswebsite.com/api.php'
 
 * SMS website username label: The username label of the SMS api. the default is 'username'. You can always change this. 
 
 * SMS website username value: The value of the username.
 
 * Password Label: The password label of the SMS api. the default is 'password'. You can always change this. 
 
 * Password Value: The value of the password.
 
 * Message Label: The message label of the SMS api. the default is 'message'. You can always change this. 
 
  * Sender Label: The sender label as required by the SMS api.
  
  * Recipient Label: The label for the phone number field.
 
 The values of the message, the sender and the recipient are expected to be set in the rules action. Simply create your rule as usual and choose the action 'send sms'. It makes perfect sense to set the message text and recipient per rules action. Rules will also provide data selection. I hope you'll enjoy this.
 
 Finally, you may want to be sure if your settings are working fine. I have added a debugging checkbox. simply check it and you'll always see the result on the page. Do not forget to disable this once everything is working fine as it will continue to show to all users of your website. 

NOTE
--------------- 
I have decided not to validate any field because of the differences in the API's I have checked while writing this module. You are required to ensure the correct data are provided in the config page and in the rules actions fields. 

Secondly,some api's expect 234 in phone number while some doesn't. It's very simply. cater for that in the recipient field in the rules action. fifo.

ACKNOWLEDGMENTS
---------------
Developed by Sherif Adigun for you. Enjoy.