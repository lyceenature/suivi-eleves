<?php 

function send_sms_admin(){
  
  
  $form = array();

  $form['send_sms_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('SMS website base url'),
    '#default_value' => variable_get('send_sms_base_url', ''),
    '#description' => t('The base url of the SMS website. (everything before the "?"). If you need explanation, see the README file included in this module.'),
    '#required' => TRUE,
  );
  
  $form['send_sms_username_label'] = array(
    '#type' => 'textfield',
    '#title' => t('SMS website username label'),
    '#default_value' => variable_get('send_sms_username_label', 'username'),
    '#description' => t('The username label as required by the SMS website'),
  );
  $form['send_sms_username_value'] = array(
    '#type' => 'textfield',
    '#title' => t('SMS website username value'),
    '#default_value' => variable_get('send_sms_username_value', ''),
    '#description' => t('The username value as required by the SMS website'),
    '#required' => TRUE,
  );
  
  $form['send_sms_password_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Password Label'),
    '#default_value' => variable_get('send_sms_password_label', 'password'),
    '#description' => t('The password label as required by the SMS website.')
  );
  $form['send_sms_password_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Password Value'),
    '#default_value' => variable_get('send_sms_password_value', ''),
    '#description' => t('The password as required by the SMS website.'),
    '#required' => TRUE,
  );
  
  $form['send_sms_message_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Message Label'),
    '#default_value' => variable_get('send_sms_message_label', 'message'),
    '#description' => t('The message label as required by the SMS website.')
  );
  $form['send_sms_sender_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender Label'),
    '#default_value' => variable_get('send_sms_sender_label', 'sender'),
    '#description' => t('The sender label as required by the SMS website.')
  );
  $form['send_sms_sender_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender Value'),
    '#default_value' => variable_get('send_sms_sender_value', variable_get('site_name', 'Default')),
    '#description' => t('The sender value as required by the SMS website.')
  );
  $form['send_sms_recipient_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipient Label'),
    '#default_value' => variable_get('send_sms_recipient_label', 'recipient'),
    '#description' => t('The recipient label as required by the SMS website.'),
  );
  $form['send_sms_show_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show message information'),
    '#default_value' => variable_get('send_sms_show_info', ''),
    '#description' => t('Enable this to show if the message has been sent or error is thrown. You should disable this option once everything is working fine')
  );

  return system_settings_form($form);
}